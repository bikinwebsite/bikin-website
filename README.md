# Google Tak Lagi Juara, Kini TikTok Jadi yang Terpopuler

Cloudflare baru saja merilis daftar platform online paling populer. Ada hal yang mengejutkan dalam daftar peringkat tersebut. Google tidak lagi menduduki peringkat pertama. Posisi Google sebagai platform online paling populer telah tergantikan oleh TikTok.

Daftar peringkat yang dirilis perusahaan Web Security tersebut didasarkan pada data laporan Cloudflare Year in Review. Data tersebut menunjukkan daftar situs online yang meraih paling banyak trafik pengunjung dalam satu tahun.

[![aus usaha saya](https://i.ibb.co/sm67Kfb/tiktok.jpg)](https://www.aus.co.id)

Situs Google.com telah mengakhiri posisinya sebagai pemimpin di peringkat Cloudflare. Padahal, platform Google tersebut juga termasuk Google Maps, Google Translate, dan juga Google News. Namun, jumlah pengunjung beberapa platform tersebut masih belum bisa mengungguli jumlah pengunjung TikTok pada tahun ini.
Tahun lalu, TikTok masih berada dalam peringkat ke-7. Namun pada akhir tahun 2021 ini, TikTok bisa mengungguli beberapa situs online terpopuler seperti Facebook, Google, Amazon, dan banyak platform terkenal lainnya.

## Beda dengan 2020, 2021 Jadi Tahunnya TikTok

Data tentang situs terpopuler di akhir tahun 2021 yang dirilis oleh Cloudflare memang mengejutkan. Hal ini karena data tersebut menunjukkan bahwa posisi pertama diduduki oleh TikTok. Padahal, pada akhir tahun 2020, TikTok masih berada di posisi ke-7.
Berikut ini adalah daftar 10 domain paling populer di akhir tahun 2021:

Tik.Tok.com
Google.com
Facebook.com
Microsoft.com
Apple.com
Amazon.com
Netflix.com
Youtube.com
Twitter.com
Whatsapp.com

## Sementara itu, daftar domain paling populer di tahun 2020 adalah sebagai berikut:

Google.com
Facebook.com
Microsoft.com
Apple.com
Netflix.com
Amazon.com
TikTok.com
Youtube.com
Instagram.com
Twitter.com

Dari data tersebut bisa diambil kesimpulan bahwa TikTok telah berhasil mengalahkan Google sebagai domain terpopuler. Di tahun ini, trafik internet tumbuh dengan pesat. Mungkin saja hal ini juga dipengaruhi oleh efek pandemi yang tidak kunjung selesai.

Karena sebagian besar masyarakat harus menghabiskan waktu dirumah, maka penggunaan internet sebagai penunjang aktivitas sehari-hari juga meningkat. Mereka tidak hanya menggunakan internet untuk bekerja dan belajar. Akan tetapi, internet juga dibutuhkan untuk mencari hiburan.
Oleh karena itu, penggunaan internet baik untuk mengakses website maupun aplikasi telah berevolusi. Platform hiburan seperti media sosial maupun situs streaming semakin diminati di masa pandemi. Mungkin karena banyak orang yang merasa bosan dirumah, sehingga mereka mencari hiburan dengan mengakses platform tersebut.

## Bukan Punya Facebook Lagi, Medsos Terpopuler Jadi Milik TikTok

Tak mengherankan jika TikTok juga telah berhasil menggeser Facebook sebagai platform media sosial terpopuler. Jumlah pengguna TikTok juga semakin hari semakin bertambah.

Selain merilis daftar situs online paling populer, Cloudflare juga merilis data tentang media sosial paling populer, platform belanja online paling populer, serta situs streaming video paling populer.
Bukanlah hal yang mengejutkan jika Amazon berhasil menjadi situs e-commerce paling populer di tahun ini. Amazon berhasil mengalahkan situs belanja online lainnya seperti Ebay, Taobao, dan juga Walmart.
Selain itu, peringkat situs streaming video terpopuler masih diraih oleh Netflix. Kemudian disusul oleh Youtube dan HBOMax.

## Selain TikTok, Ini Daftar Platform Medsos Paling Populer

Di tahun kedua pandemi ini, platform media sosial masih menduduki peringkat tertinggi sebagai platform online yang paling banyak diakses. Bahkan TikTok juga berhasil mengalahkan Facebook sebagai media sosial paling populer dan paling sering dikunjungi.

Akan tetapi, jumlah pengunjung tersebut bukanlah pengguna baru. Jadi, Facebook masih menduduki peringkat satu sebagai platform medsos dengan paling banyak pengguna. Berikut adalah daftar platform media sosial paling populer di tahun 2021:

TikTok.com
Facebook.com
Youtube.com
Twitter.com
Instagram.com
Snapchat.com
Reddit.com
Pinterest.com
LinkedIn.com
Quora.com

Sedangkan daftar peringkat platform media sosial paling populer pada tahun 2020 adalah sebagai berikut:

Facebook.com
TikTok.com
Youtube.com
Instagram.com
Twitter.com
Snapchat.com
Reddit.com
Pinterest.com
LinkedIn.com
Quora.com

Dari kedua data di atas bisa disimpulkan bahwa yang mengalami perubahan besar hanyalah peringkat Facebook dan TikTok. TikTok sebagai platform berbagi video pendek telah berhasil menggeser posisi Facebook.
Insider intelligence juga memprediksi bahwa jumlah pengguna TikTok akan mencapai angka 755 juta orang pada tahun 2022. Sementara itu, saat ini Facebook telah memiliki 2,91 juta pengguna aktif di seluruh dunia.

Selain menjadi aplikasi media sosial paling populer, TikTok juga merupakan aplikasi non-game yang paling banyak di-download di seluruh dunia. Data tersebut didasarkan pada angka rata-rata jumlah download di platform Google Play Store dan App Store.

Kepopuleran TikTok ini mungkin menjadi satu tanda tanya besar. Mengapa platform berbagai video ini bisa menjadi sangat digemari. Platform ini bisa digunakan untuk membuat, mengedit, membagikan, dan menonton video pendek yang menarik.

Selain itu, TikTok mempunyai banyak fitur unggulan yang tidak ada di media sosial lainnya. Fitur – fitur yang ada di TikTok antara lain adalah sebagai berikut. Pertama, TikTok memiliki fitur edit video. Dengan fitur ini, pengguna bisa menambahkan musik, efek, stiker, dan juga filter untuk membuat video mereka lebih menarik.
Ada juga fitur Voice Changer yang bisa digunakan untuk mengubah suara pembuat video. Dengan fitur ini, pengguna bisa mengasah kreativitas dan menambah keseruan dalam membuat video unik.
Selain itu, TikTok juga memiliki fitur Live sehingga pengguna bisa memulai siaran langsung dan ditonton oleh pengikutnya. Dengan berbagai fitur menarik tersebut, tak mengherankan jika TikTok berhasil menduduki posisi sebagai platform media sosial paling populer tahun ini.

## TikTok Populer, Sudut Pandang Apa yang Digunakan?

Dari daftar peringkat situs terpopuler yang telah dirilis oleh Cloudflare tersebut, ada beberapa perspektif yang berbeda. Masyarakat mungkin akan bertanya-tanya apakah daftar tersebut menunjukkan bahwa TikTok menjadi situs media online paling besar saat ini?

Jawabannya adalah tentu saja tidak. Ada jalan panjang yang harus dilalui TikTok agar bisa menduduki peringkat sebagai media sosial terbesar dan mengalahkan Facebook. Daftar tersebut didasarkan pada jumlah pengunjung TikTok dalam satu tahun. Dan bukan didasarkan pada jumlah pengguna TikTok.

Jadi, dari data tersebut tidak bisa diartikan bahwa jumlah pengguna TikTok lebih banyak daripada pengguna Google, Facebook, atau platform online lainnya. Berdasarkan data yang dirilis oleh Insider Intelligence (sebelumnya bernama eMarketer), jumlah pengguna TikTok telah berhasil melampaui jumlah pengguna Twitter dan Snapchat.
Akan tetapi, jumlah pengguna TikTok masih kalah dengan jumlah pengguna Facebook dan juga Instagram. Dengan kata lain, TikTok merupakan platform media sosial paling besar ke-3 di seluruh dunia setelah Facebook dan Instagram.

Jumlah pengguna TikTok mengalami pertumbuhan sebesar 59,8% pada tahun 2020. Sedangkan di tahun 2021, jumlah pengguna TikTok mengalami pertambahan sekitar 40,8%. Insider Intelligence memperkirakan bahwa jumlah pengguna TikTok akan bertambah sebesar 15,1% pada tahun 2022.

Jika perkiraan tersebut menjadi kenyataan, berarti TikTok bisa meraih 20% pengguna aktif dari keseluruhan pengguna media sosial di seluruh dunia. Jadi tidak ada salahnya jika TikTok menjadi salah satu strategi marketing melalui media sosial.

Apalagi saat ini TikTok telah memiliki fitur TikTok Shop. Fitur ini memungkinkan pengguna untuk berbelanja online melalui platform TikTok. Jadi tidak ada salahnya untuk menjadikan TikTok sebagai media pemasaran suatu bisnis.